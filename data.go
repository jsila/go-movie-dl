package moviedl

type Data struct {
	MagnetURI       string
	Name            string
	FileName        string
	SubtitlesZIPURL string
}
