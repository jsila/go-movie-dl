package moviedl

import (
	"fmt"
	"net/http"

	"strconv"

	"strings"

	libxml2 "github.com/lestrrat/go-libxml2"
	"github.com/lestrrat/go-libxml2/xpath"
)

const (
	SubtitlesSiteURL  = "http://www.yifysubtitles.com"
	SubtitleSearchURL = "http://www.yifysubtitles.com/search?q=%s"
)

func GetSubtitlesZIPURL(searchTerm string, data *Data) {
	searchTerm = strings.Replace(searchTerm, " ", "+", -1)
	res, err := http.Get(fmt.Sprintf(SubtitleSearchURL, searchTerm))
	if err != nil {
		panic(err)
	}

	doc, err := libxml2.ParseHTMLReader(res.Body)
	if err != nil {
		panic(err)
	}
	defer doc.Free()

	searchItems := xpath.NodeList(doc.Find("//li[contains(@class, 'media-movie-clickable')]//a/@href"))
	if len(searchItems) == 0 {
		panic(ErrNoMoviesFound)
	}

	subtitlesURL := fmt.Sprintf("%s%s", SubtitlesSiteURL, searchItems[0].NodeValue())

	res, err = http.Get(subtitlesURL)
	if err != nil {
		panic(err)
	}

	doc, err = libxml2.ParseHTMLReader(res.Body)
	if err != nil {
		panic(err)
	}

	ratings := xpath.NodeList(doc.Find("//table[contains(@class, 'other-subs')]//td[@class='rating-cell']//text()"))
	languages := xpath.NodeList(doc.Find("//table[contains(@class, 'other-subs')]//td[@class='flag-cell']//text()"))
	links := xpath.NodeList(doc.Find("//table[contains(@class, 'other-subs')]//td[@class='download-cell']//@href"))

	var bestSubtitles struct {
		rating int
		link   string
	}

	for i := range ratings {
		language := strings.TrimSpace(languages[i].NodeValue())
		if language != "English" {
			continue
		}
		rating, err := strconv.Atoi(strings.TrimSpace(ratings[i].NodeValue()))
		if err != nil {
			panic(err)
		}

		if rating > bestSubtitles.rating {
			bestSubtitles.rating = rating
			bestSubtitles.link = strings.TrimSpace(links[i].NodeValue())
		}
	}

	res, err = http.Get(SubtitlesSiteURL + bestSubtitles.link)
	if err != nil {
		panic(err)
	}

	doc, err = libxml2.ParseHTMLReader(res.Body)
	if err != nil {
		panic(err)
	}

	data.SubtitlesZIPURL = xpath.String(doc.Find("//a[contains(@class, 'download-subtitle')]/@href"))
}
