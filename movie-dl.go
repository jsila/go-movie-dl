package moviedl

import (
	"bytes"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"
	"sync"
)

func Get(search string, moviesFolder string) {
	data := new(Data)
	var wg sync.WaitGroup

	wg.Add(2)
	go func() {
		GetMagnet(search, data)
		GetMagnetInfo(data)

		cwd := path.Join(moviesFolder, data.Name)

		if err := os.MkdirAll(cwd, os.ModePerm); err != nil {
			panic(err)
		}

		if err := os.Chdir(cwd); err != nil {
			panic(err)
		}

		wg.Done()
	}()
	go func() {
		GetSubtitlesZIPURL(search, data)
		wg.Done()
	}()
	wg.Wait()

	if err := exec.Command("wget", data.SubtitlesZIPURL).Run(); err != nil {
		panic(err)
	}

	parts := strings.Split(data.SubtitlesZIPURL, "/")
	zip := parts[len(parts)-1]

	var out bytes.Buffer
	cmd := exec.Command("unzip", zip)
	cmd.Stdout = &out
	if err := cmd.Run(); err != nil {
		panic(err)
	}

	re := regexp.MustCompile("inflating: (.*.srt)")
	srt := re.FindStringSubmatch(out.String())[1]

	if err := os.Rename(srt, data.FileName+".srt"); err != nil {
		panic(err)
	}

	if err := exec.Command("transmission-gtk", data.MagnetURI).Run(); err != nil {
		panic(err)
	}
}
