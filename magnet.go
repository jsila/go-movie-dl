package moviedl

import (
	"errors"
	"fmt"
	"net/http"

	libxml2 "github.com/lestrrat/go-libxml2"
	"github.com/lestrrat/go-libxml2/xpath"
)

const (
	SearchURL = "https://yts.ag/browse-movies/%s/all/all/0/latest"
)

var (
	ErrNoMoviesFound = errors.New("No movies found")
)

func GetMagnet(searchTerm string, data *Data) {
	res, err := http.Get(fmt.Sprintf(SearchURL, searchTerm))
	if err != nil {
		panic(err)
	}

	doc, err := libxml2.ParseHTMLReader(res.Body)
	if err != nil {
		panic(err)
	}
	defer doc.Free()

	searchItems := xpath.NodeList(doc.Find("//a[@class='browse-movie-link']/@href"))
	if len(searchItems) == 0 {
		panic(ErrNoMoviesFound)
	}

	movieURL := searchItems[0].NodeValue()

	res, err = http.Get(movieURL)
	if err != nil {
		panic(err)
	}

	doc, err = libxml2.ParseHTMLReader(res.Body)
	if err != nil {
		panic(err)
	}

	magnets := xpath.NodeList(doc.Find("//a[contains(@class, 'magnet-download')]/@href"))

	data.MagnetURI = magnets[0].NodeValue() // 720p
}
