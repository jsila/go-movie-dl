package moviedl

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/anacrolix/torrent"
)

// IsMovieFile checks if a filename has a video extension
func IsMovieFile(filename string) bool {
	return strings.HasSuffix(filename, ".mp4") || strings.HasSuffix(filename, ".mkv")
}

// RemoveFileExtension removes Extension from filename
func RemoveFileExtension(filename string) string {
	var extension = filepath.Ext(filename)
	return filename[0 : len(filename)-len(extension)]
}

// GetMagnetInfo downloads magnet link and extracts name and name of video file it contains
func GetMagnetInfo(data *Data) {
	c, _ := torrent.NewClient(nil)
	defer c.Close()

	t, _ := c.AddMagnet(data.MagnetURI)
	<-t.GotInfo()

	data.Name = t.Info().Name

	for _, f := range t.Info().Files {
		filename := f.Path[0]
		if IsMovieFile(filename) {
			data.FileName = RemoveFileExtension(filename)
			break
		}
	}
	if err := os.Remove(".torrent.bolt.db"); err != nil {
		panic(err)
	}
}
