package main

import (
	"errors"
	"os"
	"os/user"
	"path"

	moviedl "github.com/JSila/go-movie-dl"
)

func main() {
	if len(os.Args) < 2 {
		panic(errors.New("Please provide a movie name you wish to download"))
	}

	search := os.Args[1]
	u, _ := user.Current()

	moviesFolder := path.Join(u.HomeDir, "Downloads")

	moviedl.Get(search, moviesFolder)
}
